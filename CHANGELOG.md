
6/23
- Refactored reqwest function
- Refactored contructor to not require client/campaign/form id
- Moved form/campaign/client id requirement to processForm method. This allows for more flexibility in implementation. The library can take all fields at once from the form instead of through two separate calls.

6/22
- Removed defaults fields. Never a situation where they would serve a purpose.
- Refactored the custom signal and message feature.
- Refactored down to one processForm function with several optional helper functions.
- Updated README

### v0.4.0
6/14
- Created new repo for new version of our EN js library
