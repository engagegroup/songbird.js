# SongBird v0.4.0
#### Engaging Networks API Library

## DEPRACATED

This library uses the old EN API/pages and thus is no longer supported (EOL December 2017).  The new version of the songBird library is in a separate repository (link TBD).

The new Pagebuilder API has been published.

## Background
Songbird.js is a javascript library to interact with the Engaging Networks (EN) API.  The goals of the library are as follows:

- Securely send data to EN from the browser without have to send data (possibly credit card data) through a (proxy) server first.
- Allow for an easy and low resource way of setting up an API form.
- Provide a uniform JSON based response.  The library parses the response from EN and then sends back a formatted JSON message.

The Engaging Networks API heavily relies upon matching the actual field names on the "ghost form" that is setup to receive the request (always a get/ajax request).  When building an API request, initialize a new instance of SongBird and pass in the relevant clientId, campaignId and formId.  The object will then have one main method at its disposal (and several helper methods):

    Method: #processForm
    Arguments: formFields, callback

The library parses the response from EN and returns a message indicating success or error and the whole body of what Engaging Networks passes back.  The library attempts to normalize/create a common json response to indicate success or failure, however, it will also include the native EN response object.

To submit the form, the library uses JSONP to get around CORS.  Currently, there is no way to directly POST form data in directly from the client side (browser).

### New Page Builder
There is a new version of Engaging Networks form/page builder that is not compatible with the old version.  EN has not released when the new form builder will have API functionality.

### Services API
Engaging Networks has a services api for working with data and larger data sets.  The library will be building out an interface for the services API soon.

## Getting Started

Include the songBird.js file on the page.

```html
<script src="songBird.js"/>
```

Initialize the library with a call to Songbird:

```javascript

var songbird = new SongBird();

```

### Submitting Form Fields
The library's test fields are setup with the Engaging Networks [default field names](http://support.engagingnetworks.net/default-supporter-record-tagged-field-list).  However, It's highly likely that some of your field names changed during your initial Engaging Networks system setup. Make sure you adjust accordingly.  A form object must be submitted with the basic required fields from the "ghost" form.  The library will take care of the default fields that are needed and append them to the form fields upon submit.

NOTE: There are 9 fields that are universal.  The first three can be set either through the form inputs or when you initialize the SongBird object. They are:

```javascript
this.defaults = {
  'ea.client.id': 12345,
  'ea.campaign.id': 12345,
  'ea.form.id': 1234,
  'ea_requested_action': 'ea_submit_user_form',
  'ea_javascript_enabled': 'true',
  'format': 'json',
  'ea.AJAX.submit': 'true',
  'ea.tracking.id': 'WEB API',
  'ea.submitted.page': '1',
};
```

#### Submitting the form, assuming that jQuery is being used on the page (jQuery is not required):

```javascript
  var formFields = $("#formId");
  songBird.processForm(formFields, function(signal, response) {
    // handle the response
  });

```

### Validation
Minimal validation is done at the library level in order to push that responsibility to the front end and/or backend.  However, the library will handle validation from a basic level, were form fields submitted, appending default fields, etc.  For convenience, there are helper functions that can be used to verify well formed email addresses and dollar amount formatting.

### What Responses Look Like
The library returns two arguments upon submission - signal and response.  The first is a custom message that signals whether or not the submission is a success or error. It is an attempt to provide a uniform response system.  The second argument is the whole body response from Engaging Networks.  The response from EN is useful for displaying a specific payment error or troubleshooting errors.

NOTE: Make sure you turn off/don't turn on Session Validation on the ghost form.

####Success
```
signal:
{
  response: 'success',
  message: 'The form/transaction was submitted successfully.'
}

response:
{
  sessionId: 'ef405084-9cad-43ec-b191-c8941fea67ea',
  campaignId: 12345,
  name: 'IATS Donation - US',
  campaignType: 'Net Donor',
  clientId: 1234,
  status: 'Live',
  themeId: 0,
  redirect: '',
  redirectMethod: '',
  redirectParams: {},
  row: 0,
  col: 0,
  messages: [],
  ...
 }

```

#### Payment or Form Validation Error
```
signal:
{
  response: 'error',
  message: 'There was a form and/or payment validation error. Check the response\'s messages array for more info.'
}

response:
{
  sessionId: '310939bb-5270-482b-b44e-34a7f167e96b',
  campaignId: 12345,
  name: 'IATS Donation - US',
  campaignType: 'Net Donor',
  clientId: 1234,
  status: 'Live',
  themeId: 0,
  redirect: '',
  redirectMethod: '',
  redirectParams: {},
  row: 0,
  col: 0,
  messages:
   [ { errorWithLink: 'Generic Payment Error',
       error: 'Generic Payment Error',
       message: 'Generic Payment Error',
       id: 'Generic Payment Error',
       fieldName: '' } ],
  ...
}

```

### Donation Form Example
See 'examples/donation_example.html' for a working example of how to implement.

### Advocacy Form Example
TODO

### Add New Email Address Example
See 'examples/email_example.html' for a working example of how to implement.

## API Documentation

### Setup (formId, campaignId, options OBJECT ARRAY)

### #processForm (formFields OBJECT, callback)

### Helpers

#### #validateEmail (email)
Takes in an email as an argument and returns a corresponding boolean value.

#### #validAmount (amount)
Takes a dollar amount and checks to make sure it is above zero and there are no $ signs or other extraneous information. Periods and commas are ok.  Returns a boolean value.

#### #validCreditCard
TODO

#### #mailCheck
TODO

#### #clearBit Integration
TODO

#### #parsley Integration
TODO

### Running Tests
The tests use the default engaging networks form fields and the demo account for The Engage Group.  The fake credit card numbers and amounts will only work with the IATS Demo Payment Gateway.

Run with:

```
mix tests
```


### Working on the library
Download/clone the library and npm install all dependencies.  Submit any issues and pull requests via bitbucket.

## The Engage Group
Copyright 2016 The Engage Group


[![Travis build status](http://img.shields.io/travis/seanpowell/sb4.svg?style=flat)](https://travis-ci.org/seanpowell/sb4)
[![Code Climate](https://codeclimate.com/github/seanpowell/sb4/badges/gpa.svg)](https://codeclimate.com/github/seanpowell/sb4)
[![Test Coverage](https://codeclimate.com/github/seanpowell/sb4/badges/coverage.svg)](https://codeclimate.com/github/seanpowell/sb4)
[![Dependency Status](https://david-dm.org/seanpowell/sb4.svg)](https://david-dm.org/seanpowell/sb4)
[![devDependency Status](https://david-dm.org/seanpowell/sb4/dev-status.svg)](https://david-dm.org/seanpowell/sb4#info=devDependencies)
