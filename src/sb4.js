import reqwest from 'reqwest';
import { merge, isEmpty } from 'lodash';


export class SongBird {

  constructor(clientId = 0, campaignId = 0, formId = 0, options = {}) {

    this.defaults = {
      'ea.client.id': clientId,
      'ea.campaign.id': campaignId,
      'ea.form.id': formId,
      'ea_requested_action': 'ea_submit_user_form',
      'ea_javascript_enabled': 'true',
      'format': 'json',
      'ea.AJAX.submit': 'true',
      'ea.tracking.id': 'WEB API',
      'ea.submitted.page': '1',
      // Enable field for test transactions only
      demo: false
    };

    // Extend options
    this._options = merge(this.defaults, options);
    // Is this a test? If so, add the field that allows for transactions to run through as test. Off by default.
    this.isDemo();

  }


  processForm(formFields, callback) {

    let signal;

    if (isEmpty(formFields)) {
      signal = { response: 'error', message: 'The formFields object can not be empty.' };
      callback(signal, {});
      return;
    }

    formFields = this.processFields(formFields);

    formFields = merge(this._options, formFields);

    if ((!formFields['ea.client.id']) || (!formFields['ea.campaign.id']) || (!formFields['ea.form.id'])) {
      signal = { response: 'error', message: 'Invalid clientId, campaignId or formId.' };
      callback(signal, {});
      return;
    }

    // return callback({ response: 'success', message: 'The form/transaction was submitted successfully.' }, {});
    return reqwest({
      url: 'https://netdonor.net/ea-action/action',
      method: 'get',
      data: formFields,
      type: 'jsonp',
      success: function (resp) {
        if (resp.messages[0]) {
          // There is a processing error
          signal = { response: 'error', message: 'There was a form and/or payment validation error. Check the response\'s messages array for more info.' };
        } else {
          signal = { response: 'success', message: 'The form/transaction was submitted successfully.' };
        }
        callback(signal, resp);
      },
      error: function(resp) {
        // There is a technical problem.
        signal = { response: 'error', message: 'There was a technical error with your submission. It was not sent to Engaging Networks.  Make sure the library is setup correctly.' };
        callback(signal, resp);
      }
    });
  }


// ##################################
//  Static Utility functions
// ##################################

  // Regex from http://www.regular-expressions.info/email.html
  // Checks an email for valid formatting.
  static validateEmail(email) {
    const reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    return reg.test(email);
  }

  // Checks the donation amount to make sure it is above zero and that it is indeed a number.
  static validateAmount(obj) {
    return !isNaN( parseFloat(obj) ) && isFinite( obj ) && (obj > 0);
  }


// ##################################
//  Helper functions
// ##################################

  // isNumeric tests whether a value is a number or not - regardless if it's a string or not. So 40 and "40" both return true.
  isNumeric( obj ) {
    return !isNaN( parseFloat(obj) ) && isFinite( obj );
  }

  isDemo () {
    if (this._options.demo === true) {
      this._options['ea.campaign.mode'] = "DEMO";
    }
  }

  processFields(form) {
    let fields = {};

    if (typeof form == 'object' && form.nodeName == "FORM") {
      var len = form.elements.length;
      for (var i=0; i<len; i++) {
        let field = form.elements[i];
        if (field.name && !field.disabled && field.type != 'file' && field.type != 'reset' && field.type != 'submit' && field.type != 'button') {
          fields[field.name] = field.value;
        }
      }
    } else {
      fields = form; // A shallow copy is fine here
    }

    return fields;
  }

}
