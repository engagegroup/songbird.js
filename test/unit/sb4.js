import { SongBird } from '../../src/sb4';

describe('SongBird', function() {

  console.log(SongBird);
  it('should exist', function() {
    SongBird.should.be.defined;
  });

  describe('Songbird', () => {

    let songBird, fields, amount;
    let options = { demo: true };
    const formId = 6830;
    const campaignId = 29268;
    const clientId = 1897;
    
    describe('#processForm', () => {

      beforeEach(() => {
        // Create a new Songbird object before every test.
        songBird = new SongBird(clientId, campaignId, formId, options);
      });

      amount = 85.00;
      let validFields = {
        'Country': 'United States',
        'First Name': 'Timmy',
        'Last Name': 'Twotests',
        'Email Address': 'testingengage@gmail.com',
        'Address 1': '7160 Columbia Gateway Drive',
        'Address 2': 'Suite 300',
        'City': 'Columbia',
        'State': 'MD',
        'Postcode': '21046',
        // "Payment Currency": $($this.el).find("input[name='Payment Currency']").val(),
        'Payment Type': 'Visa',
        'Credit Card Number': '4222222222222220',
        'Card Expiry Date1': '12',
        'Card Expiry Date2': '2020',
        'Card Verification Value': '1234',
        'Donation Amount': amount
      }

      let inValidCardFields = {
        'Country': 'United States',
        'First Name': 'Timmy',
        'Last Name': 'Twotests',
        'Email Address': 'testingengage@gmail.com',
        'Address 1': '7160 Columbia Gateway Drive',
        'Address 2': 'Suite 300',
        'City': 'Columbia',
        'State': 'MD',
        'Postcode': '21046',
        // "Payment Currency": $($this.el).find("input[name='Payment Currency']").val(),
        'Payment Type': 'Visa',
        'Credit Card Number': '4222222222222221',
        'Card Expiry Date1': '12',
        'Card Expiry Date2': '2020',
        'Card Verification Value': '1234',
        'Donation Amount': amount
      }

      it('requires a valid, numerical amount above zero', () => {

        SongBird.validateAmount('$40').should.be.false;

        SongBird.validateAmount(0).should.be.false;

        SongBird.validateAmount('40').should.be.true;

        SongBird.validateAmount(40).should.be.true;

      });

      it('requires a valid credit card number', () => {
        // Mod10,
      });

      it('requires an expiration date in the future', () => {
        // Check for future exp date
      });

      it('processes a donation with a valid amount, credit card and billing address fields', () => {

        songBird.processForm(validFields, function (signal, response) {
          console.log(signal, response);
          let expectedSignal = { response: 'success', message: 'The form/transaction was submitted successfully.' };
          signal.should.eql(expectedSignal);
        });

      });

      it('declines a donation with an invalid card number', () => {

        songBird.processForm(inValidCardFields, function (signal, response) {
          console.log(signal, response);
          let expectedSignal = { response: 'error', message: 'There was a form and/or payment validation error.' };
          signal.should.eql(expectedSignal);
        });

      })
    });

  });
});


